package idconv

import "gitlab.com/witchbrew/go/idutils/id"

type Converter interface {
	FromString(string) (*id.ID, error)
	ToString(*id.ID) string
}
