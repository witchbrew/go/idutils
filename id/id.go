package id

import "errors"

type ID struct {
	Value interface{}
}

func (id *ID) IsInitialized() bool {
	return id.Value != nil
}

func New() *ID {
	return &ID{}
}

var ErrNotInitialized = errors.New("ID is not initialized")


